# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

This mini project is for detecting faces with a capability of computing the distances between faces.

### Tasks ###

* Make a multiple face detection algorithm using opencv library and output the bounding box coordinates.
* Draw a circle on a face detected.
* Compute for the center of the circle from the faces.
* Draw lines between the circles so that all detected faces will be connected with lines.
* The lines being drawn should display the distance between faces and should be placed at the center of the line following its axis.

ex. if the line is horizontal, the measure should be horizontal.
if the line is tilted at theta angle, the measure should also displayed
with theta angle.